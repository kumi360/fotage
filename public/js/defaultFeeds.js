$(document).ready(function(){
	
});

function getInstaPopular(){
	addLoader();
	$.ajax({
	  url: "/getInstaPopular",
	  type: "POST",
	  data: {pageSize: 100, pageNumber: 1}
	}).done(function(data) { 
	    removeLoader();
	    fb_data = JSON.parse(data);
	    $("#modal").modal("hide");
	    var photoContainer = $('#photoContainer');
	    photoContainer.html('');
		$(fb_data).each(function(i,elm){
	      var obj = buildObj(elm);
	      photoContainer.prepend( obj, true );
	      if(i%5==0)
	        photoContainer.masonry( 'reload' );
	    });
	    $('#photoContainer').masonry( 'reload' );
	    setTimeout(function(){
	      $('#photoContainer').masonry( 'reload' );
	    }, 500);
	});
}
