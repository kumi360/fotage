$(document).ready(function(){
	getClients();
	//mapInit(34.013462833, -118.39491901, 10);
});

function getClients(){
	//+$('#txtSessionID').val()
  //var urlHost = "http://dev.jumpspoon.com";
  //var urlHost = "http://localhost";
  var urlHost = "http://174.129.29.104";
  inclass = io.connect(urlHost + '/atcourse');

  inclass.on('connect', function (data) {
  	$('#data').html('Connecting to server: ' + urlHost + '<br>');
    //inclass.send('hi!');
  })
  .on('connected', function(data){
  	$('#data').html('Connected<br>' + $('#data').html());
  	//inclass.emit('GetLocation', 'GeoLocation co-ordinates
  })
  .on('GotLocation', function(data){
  	$('#data').html('lat: ' + data.latitude + ' - long: ' + data.longitude + $('#data').html());
  	//mapInit(34.013462833, -118.39491901, 10);
  	mapInit(data.latitude, data.longitude, 17);
  })
  .on('error', function(err){
  	alert('connection refused: ' + JSON.stringify(err));
  })
}

function mapInit(lat, long, zoom){
    if(!zoom)
        zoom = 2;
    try{
        var myOptions = {
        center: new google.maps.LatLng(lat, long),
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.SATELLITE
        //mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), myOptions);
    }catch(e){
        alert('error while maps: ' + e);
    }
    
    var marker = new google.maps.Marker({
                                        position: new google.maps.LatLng(lat, long),
                                        title:"Hello World!"
                                        });
    
    // To add the marker to the map, call setMap();
    marker.setMap(map);
}