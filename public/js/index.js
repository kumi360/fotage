$(function(){

	$('#photoContainer').masonry({
	    // options
	    itemSelector : '.outterItem',
		gutterWidth:30,
		columnWidth : 320,
		/*
			columnWidth: function( containerWidth ) {
			    return containerWidth / 4;
			  },
		*/
		isFitWidth: true,
		isAnimated: true,
		animationOptions: {
	    duration: 500,
	    easing: 'linear',
	    queue: false
	  	}
	});

	var loggedIn = $('#loggedin');
	if(loggedIn.val()=="true"){

		$("#facebook_login_link").css({"display":"none"});
			$("#get_feed_link").css({"display":"none"});
			getFeed();
	}
	else{
		$("#get_feed_link").css({"display":"none"});
	}

	$("#instagram_login").live("click", function(){
		openInstaConnect();
	});

	$("#modal").modal({show:true, "backdrop":"static"});
	loadCSS('../public/css/index.css',cssLoaded);

	$('img').load(function(){
	  	$('#photoContainer').masonry( 'reload' );

	});


	$(".outterItem").live("click", function(){
		
		// Shrink and Hide
		$(".outterItem").not($(this)).removeClass('biggestItem');
		$(".innerItem").not($(this).children(".innerItem")).removeClass('biggestItem');
		$(".displayImage").not($(this).children(".displayImage")).removeClass('biggestImage');
		$('.commentContainer').not($(this).children().children('.commentContainer')).addClass('hidden');
		
		
		//check the size of the window to see if it will fit on screen properly
		var originalWidth =  $(this).children('.displayImage').width();
		var originalHeight =  $(this).children('.displayImage').width();
	
	
		if(!$(this).hasClass('biggestItem'))
		{
			// increase the size of the hidden frame and sart animating the 
			$(this).addClass('biggestItem');
			
			// do the animation
			
			$(this).children('.innerItem').addClass('biggestItem');
			$(this).children().children('.displayImage').addClass('biggestImage');
			$(this).children().children('.commentContainer').show();//.removeClass('hidden');
			
			
		/*	$(this).children().children('.displayImage').transition({ borderRadius: '90px' },1000);*/
		}
	
        else
		{
			// increase the size of the hidden frame and sart animating the 
			$(this).removeClass('biggestItem');
	
			// do the animation
			$(this).children('.innerItem').removeClass('biggestItem');
			$(this).children().children('.displayImage').removeClass('biggestImage');
			$(this).children().children('.commentContainer').hide();//.addClass('hidden');
			
		}
		
		$('#photoContainer').masonry( 'reload' );
	});
	
	
	$(window).resize(function() {
	  // change size of item so it doesnt get cut off by the smaller screen
	});
	

});

function addLoader(){
	$(".modal-footer").html("<img src='/public/img/load.gif' />");
}
function removeLoader(){
	$(".modal-footer").html("");
}
/*

$('#content').infinitescroll({

   navSelector  : "div.navigation",            
                  // selector for the paged navigation (it will be hidden)
   nextSelector : "div.navigation a:first",    
                  // selector for the NEXT link (to page 2)
   itemSelector : "#content div.post"          
                  // selector for all items you'll retrieve
});

*/

/*CSS LOADING*/
var element;
var resource = {};
var fileLoaded = false;
var timer;
function loadCSS(url,callback)
{
	element = document.createElement("link");
   	element.rel = "stylesheet";
   	element.type = "text/css";
   	element.href = url;
	resource.callback = callback;
	resource.loaded = false;
	resource.element = element;

	document.getElementsByTagName("head")[0].appendChild(element);
	fileLoaded = false;
	setTimeout(cssLoadMonitor, 10);
};

function cssLoadMonitor() {
	var stylesheets = document.styleSheets;
	
	for(var i=0;i<stylesheets.length;i++) {
		var file = stylesheets[i];
		var owner = file.ownerNode ? file.ownerNode : file.owningElement;
		  
		if(element.href == owner.href)
		{
			fileLoaded = true;
			i =stylesheets.length;
			resourceLoadComplete(owner.href);
		}
	}
	
	if(!fileLoaded)
	{
		timer =  setTimeout(cssLoadMonitor, 1);
	}
};

function resourceLoadComplete(url) {
	clearTimeout(timer);
 	resource.callback();
};

function cssLoaded()
{
	 $('#photoContainer').masonry('reload');
};

function randomPend()
{
	var rand = Math.floor(Math.random()*2)
	
	if(rand == 1)
	{
		prependTest();
	}
	
	else
	{
		appendTest();
	}	
}

// function prependTest()
// {
// 	console.log('prependTest');
	
// 	var num1 = Math.floor(Math.random()*8) + 1;
// 	var img1 = '<div class="item"><img class="displayImage" src="../public/img/default/'+num1 +'.jpg" width="270" alt="2"/></div>'
// 	var num2 = Math.floor(Math.random()*8) + 1;
// 	var img2 = '<div class="item"><img class="displayImage" src="../public/img/default/'+num2 +'.jpg" width="270"  alt="2"/></div>'
// 	var num3 = Math.floor(Math.random()*8) + 1;
// 	var img3 = '<div class="item"><img  class="displayImage" src="../public/img/default/'+num3 +'.jpg" width="270"  alt="2"/></div>'
// 	var $boxes = $(img1+img2+img3);
// 	$('#photoContainer').prepend( $boxes ).masonry( 'reload' );
// 

function buildObj(displayObj)
{

	//'+displayObj.service+"
	var itemStart = '<div class="outterItem"><div class="innerItem"><img class="displayImage" src="'+displayObj.pic_url+'" width="270"/>' ;
	var user = '<div class="userInfo"><div class="userPhoto"><img src="'+displayObj.from_pic_url+'" width="40" height="40"/></div><div class="userName">'+displayObj.from_name+'</div><div class="userService">'+displayObj.service+'</div></div>';		
	var commentsStart = '<ul class="commentContainer hidden">';
	var comments = '';
	var closeComments = '</ul>';
	var closeDiv = '</div>';	
	
	
	//<li class="comment">
	
	var listComment = '	<li class="comment">';
    var listPhoto = '<div class="commentPhoto">';
    var listImgFront =  '<img src="';
	var listImgBack = '" width="40" height="40" alt="1"/></div>'
	var listName = '<div class="commentName">';
	var listText = '<div class="commentText">';
    var closeList = '</li>';	


	if(displayObj.comments.length >0)
    {
		var curCom;	
		for(var i = 0; i < displayObj.comments.length; ++i)
		{
		  curCom= listComment + listPhoto + listImgFront + "http://graph.facebook.com/"+displayObj.comments[i].from.id+"/picture" + listImgBack + listName + displayObj.comments[i].from.name + closeDiv + listText + displayObj.comments[i].message + closeDiv + closeList;	
		  comments += curCom;
		}
		
		object = itemStart + user + commentsStart + comments + closeComments + closeDiv + closeDiv;			
	}
	
	else
	{
		var noComments = commentsStart + listComment + '<div class="noCommentName">' + 'No Comments :(' +  closeList + closeComments;
		object  = itemStart + user + noComments + closeDiv + closeDiv;
	}
				 
	return object;

	
}

// function appendTest()
// {
// 	console.log('appendTest');
	
// 	var num1 = Math.floor(Math.random()*8) + 1;
// 	var img1 = '<div class="item"><img class="displayImage" src="../public/img/default/'+num1 +'.jpg" width="270" alt="2"/></div>'
// 	var num2 = Math.floor(Math.random()*8) + 1;
// 	var img2 = '<div class="item"><img class="displayImage" src="../public/img/default/'+num2 +'.jpg" width="270"  alt="2"/></div>'
// 	var num3 = Math.floor(Math.random()*8) + 1;
// 	var img3 = '<div class="item"><img class="displayImage" src="../public/img/default/'+num3 +'.jpg" width="270"  alt="2"/></div>'
// 	var $boxes = $(img1+img2+img3);
// 	$('#photoContainer').append( $boxes, true ).masonry( 'reload' );
// }


		/*
		console.log('WIDTH B4' + $(this).children('.displayImage').width() );
		console.log('HEIGHT B4 ' + $(this).children('.displayImage').height() );
	
		
		var heightVar = $(this).children('.displayImage').height();
		console.log('WIDTH AFTER' + $(this).children('.displayImage').width() );
		console.log('HEIGHT AFTER ' + heightVar );
		
		var topVar = -1*(heightVar + 10)
		
		console.log('TOP VAR '+ topVar);
		
		topVar += 'px';
		heightVar += 'px';
	
		console.log('HEIGHT VAR '+ heightVar);
		console.log('TOP VAR '+ topVar);
		*/
		
	//	$(this).children(".commentContainer").height(heightVar);
		
	//	$(this).children(".commentContainer").top(heightVar);
		
/*
		$('.comments').addClass('hidden')
		if($(this).hasClass('visible_comments'))
		{
			console.log('was visible');
			$(this).removeClass("visible_comments");
			$(this).children(".comments").addClass("hidden");
		}
		
		else
		{
			$(this).addClass("visible_comments");
			//$(this).children(".comments").removeClass("hidden");
			$('.item').not(this).removeClass('visible_comments');
		}

*/

