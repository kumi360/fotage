var app = module.parent.app;
var express = module.parent.express;

/* *********  Set the environment here ********************/
//var mongoose = module.parent.mongoose;
/*mongodb = "mongodb://10.176.93.222/starviber";*/
//console.log('from config: ' + module.parent.mongodb);

// configs
app.configure(function() {
	app.set("views", __dirname + "/views");
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	//app.use(express.static(__dirname + "/public"));
	app.use("/public", express.static(__dirname + "/public"));
	
	app.use(express.favicon());
	app.use(express.cookieParser());
	app.use(express.session({secret: "308u534^B#$(^bu3496b3"}));
	app.set('view options', {
		layout: false
	});
	module.parent.fburl = 'graph.facebook.com';
	module.parent.instaurl = 'api.instagram.com';
	module.parent.encryption_key = "7837485764luid8763098748";
	module.parent.encryption_iv = "87394875";
	module.parent.userCookieName = 'fotusr';
	module.parent.tokenDuration = 2; // in days
});

app.configure("development", function() {
	//DB to use
	module.parent.db = "mongodb://107.22.196.216/fotage-dev";
	module.parent.fbAppID = "178352335538392";
	module.parent.fbAppSecret = "db4cd7e74bf6f1f9eb74765260c9d3e5";
	module.parent.instaAppID = "7d849a862646406990f8278a3479c1f3";
	module.parent.instaAppSecret = "de7d0b9b8a35456d92a357a1158614bc";
	
	app.use(express.errorHandler({
		dumpExceptions: true,
		showStack: true
	}));
	module.parent.port = 3000;
});

app.configure("production", function() {
	//Db to use for prod
	//Prod server public ip: 174.129.29.104
	module.parent.db = "mongodb://107.22.196.216/fotage";
	module.parent.fbAppID = "231396186979807";
	module.parent.fbAppSecret = "3caf3696c2c1e33887721e2ac5bf913a";
	module.parent.instaAppID = "fa1050921f7d4fedb15d311033119837";
	module.parent.instaAppSecret = "7842874d482c45768b52fdfd372b96b2";
	console.log('prod');
	app.use(express.errorHandler());
	module.parent.port = 80;
});

// helper functions
function encryptPassword(password, salt) {
	return crypto.createHmac("sha1", salt).update(password).digest("hex");
}