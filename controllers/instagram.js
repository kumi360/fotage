var getUser = function(request, response){
	return module.parent.models.user.getUserCookie(request, response);
}

this.authCode = function(request, response){
	response.render("instaland.ejs");
}

this.authSubmit = function(request, response){
	user = getUser(request, response);
	if(user){
		var token = request.body.instaAuthToken.split('&');
		instaToken = token[0];
		//response.end(request.body.instaAuthToken);
		module.parent.models.user.updateInstagramToken(user.id, instaToken, function(err, result){
			if(!err){
				response.end('success');
			}else{
				response.end('error' + err);
			}
		});
	}else{
		response.end('Invalid request');
	}
}