var getUser = function(request, response){
	return module.parent.models.user.getUserCookie(request, response);
}

this.getInstaPopular = function(request, response){
	module.parent.models.instaconnect.getPopular(module.parent.instaAppID, function(err, photos){
		if(!err){
			parseInstaFeed(photos, function(err, fotos){
				response.write(JSON.stringify(fotos));
				response.end();
			});
		}else{
			response.end('error');
		}
	});
}

this.getFeed = function(request, response){
	user = getUser(request, response);
	if(user){
		module.parent.models.user.getUserInfoByUid(user.id, function(err, out){
			fbtoken = out.fbtoken;
			console.log('getting fb fotos');

			module.parent.models.fbconnect.getFbPhotoFeeds(fbtoken, function(err, photos){
				if(!err){
					parseFbFeed(photos, function(err, fotos){
						response.write(JSON.stringify(fotos));
						response.end();
					});
				}else{
					response.end('error');
				}
			});
		});
	}else{
		response.end('Invalid request');
	}
}

this.getInstaFeed = function(request, response){
	user = getUser(request, response);
	if(user){
		module.parent.models.user.getUserInfoByUid(user.id, function(err, out){
			instatoken = out.instatoken;
			console.log('getting insta fotos');

			module.parent.models.instaconnect.getFeed(instatoken, function(err, photos){
				if(!err){
					parseInstaFeed(photos, function(err, fotos){
						response.write(JSON.stringify(fotos));
						response.end();
					});
				}else{
					response.end('error');
				}
			});
		});
	}else{
		response.end('Invalid request');
	}
}

var parseInstaFeed = function(instafeed, callback){
   pageSize = 1000;
   if(instafeed.length < pageSize)
   	pageSize = instafeed.length;
	var fotos = new Array;
	for(i=0; i<pageSize; i++){
		cur = instafeed[i];
		//comments = (cur.comments.data?cur.comments.data:{length:0});
		//console.log(cur.images.standard_resolution);
		foto = {
			post_id			: cur.id,
			pic_url			: cur.images.standard_resolution.url,
			pic_url_thumb	: "",
			posted_at		: cur.created_time,
			message			: "",
			link			: cur.link,
			service			: "instagram",
			from_name		: cur.user.full_name,
			from_pic_url	: cur.user.profile_picture,
			from_id			: cur.user.id,
			comments		: {length:0}
		};
		fotos.push(foto);
	}
	callback(null, fotos);
}

var parseFbFeed = function(fbfeed, callback){
   pageSize = 1000;
   if(fbfeed.length < pageSize)
   	pageSize = fbfeed.length;
	var fotos = new Array;
	for(i=0; i<pageSize; i++){
		cur = fbfeed[i];
		comments = (cur.comments.data?cur.comments.data:{length:0});
		foto = {
			post_id			: cur.id,
			pic_url			: cur.picture.replace('_s.', '_n.'),
			pic_url_thumb	: cur.picture,
			posted_at		: cur.created_time,
			message			: cur.message,
			link			: cur.link,
			service			: "facebook",
			from_name		: cur.from.name,
			from_pic_url	: "http://graph.facebook.com/" + cur.from.id + "/picture",
			from_id			: cur.from.id,
			comments		: comments
		};
		fotos.push(foto);
	}
	callback(null, fotos);
}

var sort_by = function(field, reverse, primer){

   reverse = (reverse) ? -1 : 1;

   return function(a,b){

       a = a[field];
       b = b[field];

       if (typeof(primer) != 'undefined'){
           a = primer(a);
           b = primer(b);
       }

       if (a<b) return reverse * -1;
       if (a>b) return reverse * 1;
       return 0;

   }
}