this.fbAuthLand = function(request, response){
	//response.write('called back from fb');
	//response.end();
	response.render("fbland.ejs");
}

this.fbAuthSubmit = function(request, response){
	var token = request.body.fbAuthToken.split('&');
	fbToken = token[0];
	module.parent.models.fbconnect.getFBUser(fbToken, function(d) {
		fbUser = JSON.parse(d);
		returnObj = {
			error: "",
			token: ""
		}
	    if(!fbUser.email){
	    	returnObj.error = 'No Email';
	    	response.end(JSON.stringify(returnObj));
	    }else{
			module.parent.controllers.fb.findCreateFBUser(fbToken, fbUser, function(err, user){
				if(!err){
					userToken = module.parent.models.user.createUserLoginToken(user);
					returnObj.token = userToken;
					returnObj.fbToken = fbToken;
					//set cookie
					module.parent.models.user.setUserCookie(user, response);
					response.end('Success');
				}else{
					returnObj.error = err;
	    			response.end(JSON.stringify(returnObj));
				}
			});
		 }
	});
}

this.findCreateFBUser = function(token, fbUser, callback){
	var userModel = module.parent.models.user;
	var fbUser = {
		name		: fbUser.name,
    	email		: fbUser.email,
    	password	: undefined,
    	pic_url		: "http://graph.facebook.com/" + fbUser.id + "/picture",
    	pic_large	: "http://graph.facebook.com/" + fbUser.id + "/picture?type=large",
    	fbuser_id	: fbUser.id,
    	fbtoken		: token
	};
	userModel.findOrCreateUser(
    	fbUser,
    	function(err, result){
    		if(!err){
    			if(result._id){
    				if(callback)
    					callback(null, result);
    			}else{
    				if(callback)
    					callback('Error occured');
    			}
    		}else{
    			if(callback)
    				callback(err);
    		}
    	}
    );
}