//console.log('routes');
var app = module.parent.app;
var controllers = module.parent.controllers;

/*
 * Setup all the routes you need here
 */

app.all("/", controllers.landing.land);

//app.all("/home", controllers.user.home);
app.post("/getFeed", controllers.user.getFeed);
app.post("/getInstaFeed", controllers.user.getInstaFeed);
app.post("/getInstaPopular", controllers.user.getInstaPopular);

//FB Auth
app.get("/fbAuthLand", controllers.fb.fbAuthLand);
app.all("/fbAuthSubmit", controllers.fb.fbAuthSubmit);

//Instagram Auth
app.all("/instaAuthCode", controllers.instagram.authCode);
app.all("/instaAuthSubmit", controllers.instagram.authSubmit);


//All other requests
app.all('*', function(req, res){
  //res.send('page not found', 404);
  //response.send(404);
  throw new Error('404');
});

app.error(function(err, req, res, next){
	if (err.message && err.message == '404') {
        res.render('errors/404.jade');
    } else {
        next(err);
    }
});