// init
var mongoose = module.parent.mongoose;
var mongoose_types = module.parent.mongoose_types;
mongoose_types.loadTypes(mongoose);
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var Email = mongoose.SchemaTypes.Email;
var Url = mongoose.SchemaTypes.Url;

var schema = new Schema({
	name		: {type: String,   required: false},
	email      	: {type: Email,    required: false},
	password   	: {type: String,   required: false},
	fbuser_id  	: {type: String,   required: false},
	fbtoken		: {type: String,   required: false},
	instatoken	: {type: String,   required: false},
	pic_url		: {type: String,   required: false},
	pic_thumb	: {type: String,   required: false},
	pic_large	: {type: String,   required: false},
	created_at 	: {type: Date,     required: false},
	updated_at 	: {type: Date,     required: false}
});
var model = mongoose.model('user', schema);

this.findOrCreateUser = function(user, callback){
	//name, email, password, pic_url, pic_large, pic_type, user_type, fbuserid
	module.parent.models.user.findUserByEmail(user.email, function(error, result){
		if(!result){
			mongoose.connect(module.parent.db);
			//Create new user
			var userModel = new model({
				name		: user.name,
				email		: user.email,
				password	: user.password,
				fbtoken		: user.fbtoken,
				instatoken	: (user.instatoken?user.instatoken:undefined),
				user_id		: "user_id",
				pic_url		: user.pic_url,
				pic_large	: user.pic_large,
				fbuser_id	: user.fbuserid,
				created_at	: new Date(),
				updated_at	: new Date()
			});
			//console.log(user);
			//console.log(userModel);
			userModel.save(function(err){
				mongoose.disconnect();
				if(!err){
					if(user.fbuserid){
						userModel = checkUserPic(userModel);
					}
					callback(null, userModel);
				}
				else{
					//console.log(err);
					callback(err);
				}
			});
		}else{
			//mongoose.disconnect();
			//Pull existing user
			/*if(result.fbuserid){
				result = checkUserPic(result);
			}*/
			mongoose.connect(module.parent.db);
			model.update({_id : result._id}, {$set: {fbtoken: user.fbtoken}}, function(err){
				mongoose.disconnect();
				if(!err)
					if(callback) callback(null, result);
				else
					if(callback) callback(err);
			});
			//callback(null, result);
		}
	});
}

this.updateInstagramToken = function(uid, token, callback){
	mongoose.connect(module.parent.db);
	model.update({_id : uid}, {$set: {instatoken: token}}, function(err){
		mongoose.disconnect();
		if(!err)
			if(callback) callback(null, true);
		else
			if(callback) callback(err);
	});
}

var checkUserPic = function(user){
	if(user.pic_type){
		if(user.pic_type == "fb"){
			user.pic_url = "http://graph.facebook.com/" + user.fbuser_id + "/picture";
			user.pic_large = "http://graph.facebook.com/" + user.fbuser_id + "/picture?type=large";
		}
	}
	return user;
}

this.getUserInfoByUid = function(uid, callback){
	mongoose.connect(module.parent.db);
	var user = model.findById(uid, function(error, result){
		mongoose.disconnect();
		if(!error){
			out = checkUserPic(result);
			callback(null, out);
		}
		else{
			console.log(error);
			callback(error);
		}
	});
}

this.findAUser = function(callback){
	mongoose.connect(module.parent.db);
	var users = model.find({}, function(error, result){
		mongoose.disconnect();
		callback(result);
	});
}

this.findUserByEmail = function(email, callback){
	mongoose.connect(module.parent.db);
	model.findOne({email: email}, function(error, result){
		mongoose.disconnect();
		if(!error){
			if(callback) callback(null, result);
		}else{
			if(callback) callback(error);
		}
	});
}



this.getTotalUsers = function(callback){
	mongoose.connect(module.parent.db);
	model.count({}, function(error, result){
		mongoose.disconnect();
		if(!error)
			callback(null, result);
		else
			callback(error);
	});
}

this.createUserLoginToken = function(user){
	//Create the cookie token for Web browsers
	// userid + name + tokenExpirationDate
	//console.log(user);
	var expDate = new Date();
	expDate.setDate(expDate.getUTCDate() + module.parent.tokenDuration);
	expDate = Math.round(expDate.getTime() / 1000);
	var token = user._id + '|' + user.name + '|' + expDate;
	token = module.parent.models.utils.encrypt(token);
	return token;
}

this.parseUserToken = function(encryptCookie){
	token = module.parent.models.utils.decrypt(encryptCookie);
	token = token.split('|');
	user = {
		id			: token[0],
		name		: token[1],
		expDate		: token[2]
	};
	expDate = new Date(user.expDate*1000);
	user.expDateString = expDate;
	curDate = new Date();
	if(expDate > curDate)
		user.valid = true;
	else
		user.valid = false;
	return user;
}

this.createWebUserLoginToken = function(user){
	//Create the cookie token for Web browsers
	console.log(user["name"]);
	var token = user._id + '|' + user.name + '|' + user.pic_url+ '|' + user.pic_large;
	console.log(token);
	return module.parent.models.utils.encrypt(token);
}

this.setUserCookie = function(user, response){
	//console.log(user);
	var cookie = this.createUserLoginToken(user);
	var cookieDate = new Date();
	cookieDate.setDate(cookieDate.getDate() + 30);
	response.cookie(module.parent.userCookieName, cookie, { expires: cookieDate, httpOnly: true, path: '/' });
}

this.clearUserCookie = function(response){
	response.clearCookie(module.parent.userCookieName);
	response.redirect("/");
}

this.getUserCookie = function(request){
	userCookie = request.cookies[module.parent.userCookieName];
	if(userCookie){
		cookie = this.parseUserToken(userCookie);
		if(cookie.valid)
			return cookie;
		else
			return undefined;
	}else{
		return undefined;
	}
}

