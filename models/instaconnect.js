var https = require('https');
var options = {
  host: module.parent.instaurl,
  port: 443,
  path: '',
  method: 'GET'
};

this.getPopular = function(clientid, callback){
	options.path = '/v1/media/popular?client_id=' + clientid;
	var req = https.request(options, function(res) {
		var fullResponse = "";
    	res.setEncoding('utf8');
		res.on('data', function(d) {
			fullResponse += d;
		});
		res.on('end', function(){
			fbPhotosJson = JSON.parse(fullResponse);
			callback(null, fbPhotosJson.data);
		});
	});
	req.end(); //End fb request
	req.on('error', function(e) {
		callback(e);
	});
}

this.getFeed = function(token, callback){
	options.path = '/v1/users/self/feed?access_token=' + token;
	var req = https.request(options, function(res) {
		var fullResponse = "";
    	res.setEncoding('utf8');
		res.on('data', function(d) {
			fullResponse += d;
		});
		res.on('end', function(){
			fbPhotosJson = JSON.parse(fullResponse);
			callback(null, fbPhotosJson.data);
		});
	});
	req.end(); //End fb request
	req.on('error', function(e) {
		callback(e);
	});
}
this.getInstaToken = function(code, callback){
	var options = {
	  host: module.parent.instaurl,
	  port: 443,
	  path: '',
	  method: 'POST'
	};
	var querystring = require('querystring');
	  var post_data = querystring.stringify({
	      'client_id' : module.parent.instaAppID,
	      'client_secret': module.parent.instaAppSecret,
	      'grant_type': 'authorization_code',
	      'redirect_uri': 'http://localhost:3000/instaAuthSubmit',
	      'code': code
	  });
	  console.log(post_data);
	  options.header =  {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': post_data.length
      };

	options.path = '/oauth/access_token';
	var req = https.request(options, function(res) {
		var fullResponse = "";
    	res.setEncoding('utf8');
		res.on('data', function(d) {
			fullResponse += d;
		});
		res.on('end', function(){
			console.log(fullResponse);
			callback(fullResponse);
			/*fbPhotosJson = JSON.parse(fullResponse);
			callback(null, fbPhotosJson.data);*/
		});
	});
	req.end(); //End fb request
	req.on('error', function(e) {
		callback(e);
	});
}