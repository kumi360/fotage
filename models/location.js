// init
var mongoose = module.parent.mongoose;
var mongoose_types = module.parent.mongoose_types;
var Schema = mongoose.Schema;
mongoose_types.loadTypes(mongoose);

var schema = new Schema({
	location	: {},
	created_at 	: {type: Date,     required: true},
	updated_at 	: {type: Date,     required: true}
});
var model = mongoose.model('location', schema);

this.add = function(data, callback){
	mongoose.connect(module.parent.db);
	var loc = new model({
		position	: data.position,
		accurate	: data.accurate,
		created_at	: new Date(),
		updated_at	: new Date()
	});
	loc.save(function(err){
		mongoose.disconnect();
		if(!err){
			callback(null, loc);
		}
		else{
			callback(err);
		}
	});
}
