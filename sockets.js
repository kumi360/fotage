var app = module.parent.app;
var controllers = module.parent.controllers;
var io = require("socket.io").listen(app);
io.configure(function () { 
	io.set("transports", ["xhr-polling"]); 
	io.set("polling duration", 10); 
	io.set("log level", 2);
});

/* start socket */
var parse_cookies = function(_cookies){
    var cookies = {};

    _cookies && _cookies.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        cookies[ parts[ 0 ].trim() ] = ( parts[ 1 ] || '' ).trim();
    });

    return cookies;
}

var atcourse = io
  .of('/atcourse').authorization(function (handshakeData, callback) {
		callback(null, true);
	})
  .on('connection', function (socket) {
  	socket.emit('connected', {});
  	console.log('initiating client to start syncing');
  	var updates;
  	socket.on('SendLocation', function(location){
  		console.log(location);
	  	socket.broadcast.to(socket.room).emit('GotLocation', location);
	})
	.on('disconnect', function(){
		if(socket.room){
			console.log('user disconnected');
		}
	})
	.on('SaveLocation', function(data, fn){
		//console.log('Got location: ' + JSON.stringify(data));
		//Save location info here
		module.parent.models.location.add(data, function(err, loc){
			if(!err){
				fn(null, loc);
			}else{
				fn(err);
			}
		});
	});
  })  
  .on('message', function(frm, msg){
  	console.log('message from client: ' + msg);
  })
  .on('error', function (reason){
  	console.error('Unable to connect Socket.IO', reason);
});