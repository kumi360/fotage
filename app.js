// modules
var express = module.express = require("express");
var util = require("util");
var mongoose = module.mongoose = require("mongoose");
var mongoose_types = module.mongoose_types = require("mongoose-types");

//var models = require("./models");
var crypto = require("crypto");
//var everyauth = module.everyauth = require("everyauth");

// init
var app = module.app = express.createServer();
require('./env');
console.log('Env: ' + module.env);
app.settings.env = module.env;
//Enable io socket later 
//var server = io.listen(app);

process.on("uncaughtException", function (e) {
	console.log("*****************************************************");
	console.log("stack: " + e.stack);
	console.log("*****************************************************");
	/*console.log("Error is %j", e); 
    console.log("Error is %j", e.message ); 
  	console.log("Error is %j", util.inspect(e, true) );*/ 
	/*console.log("Caught Exception: " + error);
	console.log("Caught Exception: " + error);*/
});

//Configs
require('./config');

// Load all the controllers
var controllers = module.controllers = new Array;
require("fs").readdirSync(__dirname + '/controllers').forEach(function(file){
	curFile = file.replace('.js', '');
	if(curFile != 'index'){
		controllers[curFile] = require(__dirname + '/controllers/' + file);
	}
});

// Load all the models
var models = module.models = new Array;
require("fs").readdirSync(__dirname + '/models').forEach(function(file){
	curFile = file.replace('.js', '');
	if(curFile != 'index'){
		models[curFile] = require(__dirname + '/models/' + file);
	}
});

//routes
require('./routes');

require('./sockets');

// main
app.listen(module.port);
console.log(
	"Express server listening on port %d in %s mode",
	module.port,
	app.settings.env
);
